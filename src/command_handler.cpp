#include "command_handler.h"
#include <cstring>
#include <string>
#include <iostream>
#include <thread>

int command_handler::handle_command(char *arg) {
    ++commands_handled;
    if (strcmp(arg,"--help") == 0) {
        return handle_help();
    }
    if (strcmp(arg,"--verbose") == 0) {
        return handle_verbosity();
    }
//    if (strcmp(arg,"--seq") == 0) {
//        return handle_strategy(true);
//    }
//    if (strcmp(arg,"--pre") == 0) {
//        return handle_strategy(false);
//    }
    if (arg[0] == '-') {
        if (arg[1] == '-') {
            // unknown command
            return 2;
        }
        else {
            return handle_threads(arg);
        }
    }
    else {
        return handle_path(arg);
    }
}

int command_handler::handle_help() {
    if (commands_handled > 1) {
        std::cout << "If a help flag is set, it should be the first and only one, btw...\n\n";
    }
    std::cout << "-x\n";
    std::cout << "To set the amount of threads manually, write -x (replace x with the desired amount of threads).\n";
    std::cout << "If the amount of threads is <= 0 (default), the program will try to get the amount of cores on the system and set the correct value automatically.\n\n";
    std::cout << "--verbose\n";
    std::cout << "Prints additional info. Used mostly to receive feedback during the parallel processing.\n\n";
//    std::cout << "--seq (parallel only)\n";
//    std::cout << "One of the two possible strategies.\n";
//    std::cout << "Vector of all paths to read is constructed. All paths are sorted by the file size. Each thread locks the vector, looks up a path, removes it from the vector, releases the vector, processes the file.\n";
//    std::cout << "+ Should result in effective work load balancing.\n";
//    std::cout << "- If short files are read, frequent locking and releasing of the vector slows the program down.\n\n";
//    std::cout << "--pre (parallel only, default)\n";
//    std::cout << "Second of the two possible strategies.\n";
//    std::cout << "Vector of all paths to read is constructed. All paths are sorted by the file size. Each thread is then given a starting index from which to start. After reading that path, it will jump as much indexes as there are threads and continue until the index is out of bounds.\n";
//    std::cout << "+ No locking and unlocking of a lock during path lookup.\n";
//    std::cout << "- If for some reason one threads falls behind, uneven work load balancing happens.\n\n";
    std::cout << "Anything else is considered a path to a file/directory.\n";
    std::cout << "Directory path will read only the top-level .txt files.\n";
    std::cout << "One file can be read more than once by adding it as parameter multiple times. This may cause problems with parallel processing though? But at the same time it is just reading, so maybe not? I dunno. Safest way would be just to copy-paste the files.\n";
    std::cout << "For maximum compatibility, read only files containing ASCII characters with proper encoding.\n\n";
    std::cout << "Should be able to work both with absolute and relative paths.\n\n";
    std::cout << "Results are stored in a file named 'wc_results.txt' at the program's location.\n\n";
    std::cout << "Author's note:\n";
    std::cout << "Best parallelism results were achieved by using 2 threads (instead of the maximum of 8 for me) and only when reading large files.\n";
    std::cout << "Another benefit of the single threaded version is that it does not look up file sizes and does not sort the paths based on them.\n";
    system("pause");
    return 1;
}

int command_handler::handle_threads(char *arg) {
    try {
        threads = std::stoi(std::string(arg).substr(1));
        return 0;
    }
    catch (const std::exception& e) {
        std::cout << e.what();
        return 3;
    }
}

int command_handler::handle_path(char *arg) {
    paths.emplace_back(arg);
    return 0;
}

void command_handler::check_threads() {
    if (threads <= 0) {
        threads = std::thread::hardware_concurrency();
        if (threads == 0) {
            threads = 1;
            std::cout << "Number of threads was not manually set / was set to some prank number AND automatic detection"
                         "has failed! Setting number of threads to one!" << std::endl;
        }
    }
}

//int command_handler::handle_strategy(bool seq) {
//    this->seq = seq;
//    return 0;
//}

int command_handler::handle_verbosity() {
    verbose = true;
    return 0;
}
