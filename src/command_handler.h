#ifndef SEMESTRALKA_COMMANDS_HANDLER_H
#define SEMESTRALKA_COMMANDS_HANDLER_H

#include <vector>
#include <string>

class command_handler {
    unsigned int commands_handled = 0;
    int& threads;
    // Use seq strategy? If false; use the second strategy - pre;
    bool& seq; // Obsolete
    bool& verbose;
    std::vector<std::string>& paths;

    int handle_help();
    int handle_threads(char* arg);
    int handle_path(char* arg);
    int handle_verbosity();

    //int handle_strategy(bool seq);

public:
    command_handler(int& threads, std::vector<std::string>& paths , bool& seq, bool& verbose) : threads(threads), paths(paths), seq(seq), verbose(verbose) {};
    int handle_command(char* arg);
    void check_threads();
};

#endif //SEMESTRALKA_COMMANDS_HANDLER_H
