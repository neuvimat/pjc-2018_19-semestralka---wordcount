#include <iostream>
#include <fstream>

#include <vector>
#include <string>
#include <chrono>

//#include <clocale>

#include "wordmap.h"
#include "command_handler.h"
#include "wordcount.h"

// fixme: handling anything other than ASCII revealed itself as a complete cancer, feature dropped
// fixme: reads only ANSI .txt files on my poor Windows 10 notebook

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

int main(int argc, char *args[]) {
//    setlocale(LC_ALL, "en_US.UTF-8"); // nevypisuje ř, č, ...
//    std::locale::global(std::locale("en_US.UTF-8")); // radsi rovnou hodi error (ve visual studio 2017 nehodi error v release verzi...)
//    std::cout << "User-preferred locale setting is " << std::locale("").name().c_str() << "\n"; // nevypise vubec nic, online tool tohle zvladne, vcetne ř,č, ihned jen pres std::cout -  http://cpp.sh/

    auto start = std::chrono::high_resolution_clock::now();

    int threads = 0;
    bool seq = false; // Obsolete, randomly was crashing about 20% of the time, but I am too lazy to refactor the code and remove the reference to this variable from all the places
    bool verbose = false;
    std::vector<std::string> paths;
	
    { // Encapsulating this in brackets should throw away the command_handler instance once it is done,freeing the stack
        command_handler ch(threads, paths, seq, verbose);
        if (argc == 1) {
            std::cout << "Rerun with --help for help.\n";
            system("pause");
        } else {
            for (int i = 1; i < argc; ++i) {
                int parse_status = ch.handle_command(args[i]);
                if (parse_status == 1) {
                    // Help command was received (--help), stop reading further, print help, end the program
                    // btw: the help message is printed within the command parsing, no need to print it here
                    // yes, i know it is not unified with the other errors, that get printed here, but w/e
                    return 0;
                }
                if (parse_status == 2) {
                    // unknown command
                    std::cout << "Unknown command: '" << args[i] << "'!";
                    return 0;
                }
                if (parse_status == 3) {
                    // error reading amount of threads
                    std::cout << "Error parsing the specified amount of threads to use.";
                    return 0;
                }
            }
            ch.check_threads(); // Ensures threads is >= 1;
        }
    }
	
    // Because Visual Studio with CMake is retarded, use the code below when running the app since including start-up
    // params seems to be impossible task in VS. Don't forget to comment the code above to disable command parsing
    // when using the code below as simulation of parameters.
	// Note: test were run by "simulating" the launch parameters. But since the processing of the parameters, once done, should not have
	// any impact on performance, I'm fine with that. (At worst we can simply subtract, let's say, 5ms from each time stamp)
    /*
	threads = 8;
    seq = false; // obsolete now, keep it for legacy reasons
    verbose = true;
	paths = {
            //R"(C:\Users\Kappanata\Desktop\Test Data\short_many)",
			//R"(C:\Users\Kappanata\Desktop\Test Data\short)",
			//R"(C:\Users\Kappanata\Desktop\Test Data\min)",
			//R"(C:\Users\Kappanata\Desktop\Test Data\long_many)",
			R"(C:\Users\Kappanata\Desktop\Test Data\long)",

    };
	*/

    if (!paths.empty()) {
        wordcount wc{paths, threads, seq, verbose};
        wc.count();

        std::cout << "\nNeeded " << to_ms(std::chrono::high_resolution_clock::now() - start).count() << " ms to finish.\n";
        std::cout << "Now saving the results to 'wc_results.txt' at the path of the .exe\n";

        wc.save_results("wc_results.txt");

        std::cout << "Done writing the results.\n";
        std::cout << "Needed " << to_ms(std::chrono::high_resolution_clock::now() - start).count() << " ms to finish including writing the results into a file.\n";

		system("pause");
    }
    else {
        std::cout << "No viable paths found. Ending the program.";
    }

    return 0;
}
