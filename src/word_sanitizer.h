#ifndef SEMESTRALKA_WORD_SANITIZER_H
#define SEMESTRALKA_WORD_SANITIZER_H

#include <cstring>
#include <algorithm>
#include <locale>
#include <cctype>
#include <iostream>
#include <cwctype>

// fixme: the question is, maybe we care about -,+, brackets, etc... if they are standing alone?
// but they are not words, so... I guess we do not care

#define unwanted "{}[](),.:-*/-+=|?><!\"\'`~@#$%^&;"

bool is_unwanted(const int& c) {
    // Borrowed from symcalc
    return std::strchr(unwanted, c) != nullptr;
};

void sanitize_word(std::string& word) {
    // fixme: std::isspace may crash the program if character that is not ASCII is read
    // Trim unwanted characters from left
    word.erase(word.begin(), std::find_if(word.begin(), word.end(), [](char ch) {
        return !(std::isspace(ch) || is_unwanted(ch));
    }));
    // Trim unwanted characters from right
    word.erase(std::find_if(word.rbegin(), word.rend(), [](char ch) {
        return !(std::isspace(ch) || is_unwanted(ch));
    }).base(), word.end());
    // Make lowercase
    std::transform(word.begin(), word.end(), word.begin(), ::tolower);
}

#endif //SEMESTRALKA_WORD_SANITIZER_H
