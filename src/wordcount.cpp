#include "wordcount.h"
#include "wordmap.h"
#include "wordmap_synchronized.h"
#include "word_sanitizer.h"

#include <iostream>
#include <fstream>
#include <experimental/filesystem>

void wordcount::count() {
    if (threads == 1) {
        count_single(actual_paths);
    } else {
        count_multiple(threads, actual_paths);
    }
}

void wordcount::count_single(std::vector<std::string> &paths) {
    discover_directories();

    map = std::make_unique<wordmap>(wordmap{});
    std::cout << "Running the program in single threaded version.\n\n";

    for (const auto &path : paths) {
        int thread_id = 1; // Can't use literal inside a function because we pass by ref or smth and IDE screams
        read_file_and_merge(path, thread_id);
    }
}

void wordcount::count_multiple(int _threads, std::vector<std::string> &paths) {
    discover_directories_parallel();

    map = std::make_unique<wordmap_synchronized>();

    if (paths.empty()) {
        std::cout << "No viable paths found!" << std::endl;
        return;
    }

    // Limit the maximum amount of threads
    if (_threads > paths.size()) {
        _threads = paths.size();
    }

    std::vector<std::thread> threads;
    threads.reserve(_threads);

    std::cout << "Instantiating " << _threads << " threads.\n\n";
    for (int i = 0; i < _threads; ++i) {
        threads.emplace_back(std::thread{[&,i]() mutable { // I think that mutable does not have to be here, but I'm desperate in looking for a solution to random crash in every 10th run of the program
            if (verbose) printf("Thread #%i: initialized.\n", i);
            int thread_it = i;
            int current = i;
            while (true) {
                if (verbose) printf("Thread #%i: working on a next file! (Current index: %i, size: %i)\n", thread_it, current, actual_paths.size()-1);
                if (current < actual_paths.size()) {
                    std::string& path = actual_paths[current];
                    current += _threads;
                    read_file_and_merge(path, thread_it);
                } else {
                    if (verbose) printf("Thread #%i: current index is out of bounds!\n", thread_it);
                    break;
                }
            }
            if (verbose) printf("Thread #%i: should die now.\n", thread_it);
        }});
    }

    std::cout << "Made threads, waiting for them.\n";
    for (std::thread &thread : threads) {
        thread.join();
    }
    std::cout << "All threads are done.\n";
}

void wordcount::save_results(const std::string &path) {
    std::ofstream file;
    file.exceptions();
    try {
        file.open(path);
        if (file.is_open()) {
            file << *this;
            file.close();
        } else {
            std::cout << "Error writing results to 'wc_results.txt'\n";
        }
    }
    catch (const std::exception& e) {
        std::cout << "Error writing results to 'wc_results.txt' " << e.what() << "\n";
    }
}

void wordcount::read_file_and_merge(const std::string &path, int& thread_id) {
    wordmap partial_map{};

    std::string line;
    std::ifstream file;
    file.exceptions();
    try {
        file.open(path);
        if (file.is_open()) {
            if (verbose) printf("Thread #%i: %s - reading the file.\n", thread_id, path.c_str());
            while (file >> line) {
//                std::cout << "someone is reading\n";
                if (line.empty()) {
                    continue;
                }
                sanitize_word(line);
                // Check if the word survived the process
                if (!line.empty()) {
                    map->put(line, 1);
                }
            }
            if (verbose) printf("Thread #%i: %s - done reading the file. Closing it.\n", thread_id, path.c_str());
            file.close();
            if (verbose) printf("Thread #%i: %s - merging results.\n", thread_id, path.c_str());
            map->merge(partial_map); // Polymorphism will make sure to lock the map until the merge finishes
        } else {
            std::string error{};
            errors.emplace_back(path + ": Could not read file!");
        }
        if (verbose) printf("Thread #%i: %s - done with merging.\n", thread_id, path.c_str());
    }
    catch (std::exception &e) {
        printf("Error in thread #%i during read: %s", thread_id, e.what());
        errors.emplace_back(path + ": " + e.what());
    }
}

std::ostream &operator<<(std::ostream &os, wordcount const &wcount) {
    os << "Word count complete. " << wcount.threads << " threads were used!" << std::endl;
    if (!wcount.errors.empty()) {
        os << std::endl << "Encountered following errors during counting:" << std::endl;
        for (const std::string &err : wcount.errors) {
            os << "    " << err << std::endl;
        }
    }
    os << std::endl << "Word statistics below: " << std::endl;
    os << *(wcount.map);
    return os;
}

void wordcount::discover_directories() {
    std::cout << "Looking for directories and viable files in them.\n";
    std::vector<std::string> to_add{};
    for (auto it = actual_paths.begin(); it != actual_paths.end();) {
        // Must use experimental since I do not know how to switch to C++ 17
        if (std::experimental::filesystem::is_directory(*it)) {
            std::string path = *it;

            for (const auto &entry : std::experimental::filesystem::directory_iterator(path)) {
                if (std::experimental::filesystem::path(entry).extension() == ".txt") {
                    to_add.emplace_back(entry.path().string());
                }
            }
            it = actual_paths.erase(it);
        } else if (std::experimental::filesystem::path(*it).extension() != ".txt") {
            it = actual_paths.erase(it);
        }
        if (it != actual_paths.end()) {
            ++it;
        }
    }
    if (!to_add.empty()) {
        actual_paths.reserve(actual_paths.size() + to_add.size()); // preallocate memory
        actual_paths.insert(actual_paths.end(), to_add.begin(), to_add.end());
    }
}

void wordcount::discover_directories_parallel() {
    std::cout << "Looking for directories and viable files in them. ('Optimized' for parallel version.)\n";

    // Insides of this methods are not DRY, but w/e

    std::vector<std::pair<std::string, int>> temp;
    for (auto it = actual_paths.begin(); it != actual_paths.end();) {
        // Must use experimental since I do not know how to switch to C++ 17
        if (std::experimental::filesystem::is_directory(*it)) {
            std::string path = *it;

            for (const auto &entry : std::experimental::filesystem::directory_iterator(path)) {
                if (std::experimental::filesystem::path(entry).extension() == ".txt") {
                     temp.emplace_back(entry.path().string(), std::experimental::filesystem::file_size(entry));
                }
            }
            it = actual_paths.erase(it);
        }
        else if (std::experimental::filesystem::path(*it).extension() != ".txt") {
            errors.emplace_back(*it + ": File is not .txt!");
            it = actual_paths.erase(it);
        }
        else {
            if (std::experimental::filesystem::exists(*it)) {
                temp.emplace_back(*it, std::experimental::filesystem::file_size(*it));
            }
            else {
                errors.emplace_back(*it + ": File does not exists!");
            }
        }
        if (it !=actual_paths.end()) {
            ++it;
        }
    }

    std::sort(temp.begin(), temp.end(), [](std::pair<std::string, int>& a, std::pair<std::string, int>& b) {
        return a.second > b.second;
    });

    actual_paths.clear();
    for (const auto& entry : temp) {
        actual_paths.emplace_back(entry.first);
    }
}