#include <utility>
#include <mutex>

#ifndef SEMESTRALKA_WORDCOUNT_SINGLE_H
#define SEMESTRALKA_WORDCOUNT_SINGLE_H

#include <vector>
#include "wordmap.h"

class wordcount {
private:
    std::unique_ptr<wordmap> map;
    int threads;
    bool seq;
    bool verbose;
    std::vector<std::string> errors;
    std::vector<std::string> actual_paths;

    void count_single(std::vector<std::string>& paths);
    void count_multiple(int _threads, std::vector<std::string>& paths);
    void read_file_and_merge(const std::string &path, int& thread_id);
    void discover_directories();
    // The discovery itself is not parallel! Instead the data is 'optimized' for parallel processing
    void discover_directories_parallel();

    //void pre_strategy(std::vector<std::thread>& vec_threads, int _threads);
    //void seq_strategy(std::vector<std::thread>& vec_threads, int _threads, std::mutex& mutex);

    friend std::ostream& operator<<(std::ostream& os, wordcount const& wcount);

public:
    void count();
    void save_results(const std::string &path);
    // Input the paths by copy as we will slowly reduce the vector, but we may want to keep the source paths for later
    wordcount(std::vector<std::string> paths, int& threads, bool seq, bool verbose) : actual_paths(std::move(paths)), threads(threads), seq(seq), verbose(verbose) {};
};


#endif //SEMESTRALKA_WORDCOUNT_SINGLE_H
