#include "wordmap.h"
#include <iostream>
#include <algorithm>

void wordmap::merge(wordmap const &other) {
    for (const auto &entry: *other) {
        (this->map)[entry.first] += entry.second;
    }
}

void wordmap::put(string word, unsigned int amount) {
    (this->map)[word] += amount;
}

std::ostream &operator<<(std::ostream &os, wordmap const& wmap) {
    for (const auto &entry : *wmap) {
        os << entry.first << ": " << entry.second << "x" << std::endl;
    }
    return os;
}

wordmap_t const &wordmap::operator*() const {
    return map;
}
