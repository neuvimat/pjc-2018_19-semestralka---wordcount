#ifndef SEMESTRALKA_WORDMAP_H
#define SEMESTRALKA_WORDMAP_H

#include <string>
#include <map>
#include <memory>
#include <iostream>

using string = std::string;
using wordmap_t = std::map<string, size_t>;

class wordmap {
protected:
    wordmap_t map;
public:
    wordmap() : map(wordmap_t()) {};

    virtual void merge(wordmap const& other);
    void put(string word, unsigned int amount);
    friend std::ostream& operator<<(std::ostream& os, wordmap const& wmap);

    const wordmap_t& operator*() const;
};


#endif //SEMESTRALKA_WORDMAP_H
