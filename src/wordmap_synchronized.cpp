#include <mutex>
#include <iostream>

#include "wordmap_synchronized.h"

void wordmap_synchronized::merge(wordmap const &other) {
//    printf("Sync merge - called\n");
    std::lock_guard<std::mutex> l{mutex};
//    printf("Sync merge - got lock guard\n");
    // the copy paste does not have to be here, it could be theoretically replaced by wordmap::merge(other);
    // but it was interfering with debug messages before when testing
    for (const auto &entry: *other) {
        (this->map)[entry.first] += entry.second;
    }
//    printf("Sync merge - ended, releasing lock guard\n");
}
