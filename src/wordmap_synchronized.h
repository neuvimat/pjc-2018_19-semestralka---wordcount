#ifndef SEMESTRALKA_WORMAP_SYNCHRONIZED_H
#define SEMESTRALKA_WORMAP_SYNCHRONIZED_H

#include <mutex>
#include "wordmap.h"

/**
 * Exposes one more method which handles synchronized merge
 * Does not prevent the other map from changing during the merging, as we do not need that in our use case
 */
class wordmap_synchronized : public wordmap {
public:
    wordmap_synchronized() {};

    void merge(wordmap const& other) override;
private:
    std::mutex mutex;
};

#endif //SEMESTRALKA_WORMAP_SYNCHRONIZED_H
